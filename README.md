**Instructions
--------------------------**


1: git clone https://gitlab.com/PrashanthGowda/master-mind.git

2: cd master-mind

3: **To run test on master-mind**

    python test_master_mind.py

4: **to play master-mind on CLI**

    python MasterMindcli.py
