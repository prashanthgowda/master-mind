from master_mind import MasterMind

game = MasterMind()

# TEST CASE FOR ANSWER GENERATION
for i in range(-5, 15):
    if i < 2 or i > 9:
        assert {'is_set': False} == game.initilize_answer(i)
    else:
        initialised_answer = game.initilize_answer(i)
        assert initialised_answer['is_set'] is True

game._answer = 12345


# TEST CASE FOR INVALID USER INPUT
assert game.play({'user_guessed_number': 'abc', 'attempts': 1}) \
        == {'user_guessed_number': 0, 'cows': 0, 'bulls': 0,
            'attempts': 0, 'message': 'invalid user input',
            'is_user_guess_correct': False,
            'is_user_input_valid': False}


# TEST CASE FOR INVALID PARAMETERS
assert game.play({'user_guessed_number': 'abc'}) \
        == {'message': 'Error in parameters format',
            'is_parameters_valid': False}

assert game.play({}) \
        == {'message': 'Error in parameters format',
            'is_parameters_valid': False}

assert game.play({'user_guessed_number'}) \
        == {'message': 'Error in parameters format',
            'is_parameters_valid': False}

assert game.play([]) \
        == {'message': 'Error in parameters format',
            'is_parameters_valid': False}

assert game.play({'user_guessed_number': 'abc', 'key': ''}) \
        == {'message': 'Error in input', 'is_parameters_valid': False}

assert game.play('reset') \
        == {'message': 'Error in parameters format',
            'is_parameters_valid': False}


# TEST CASE FOR USER INPUT OUT OF RANGE

game._answer = 12345

assert game.play({'user_guessed_number': 0, 'attempts': 1})\
        == {'user_guessed_number': 0, 'cows': 0, 'bulls': 0, 'attempts': 0,
            'message': 'invalid user input', 'is_user_guess_correct': False,
            'is_user_input_valid': False}

assert game.play({'user_guessed_number': 100000000, 'attempts': 1})\
        == {'user_guessed_number': 0, 'cows': 0, 'bulls': 0, 'attempts': 0,
            'message': 'invalid user input', 'is_user_guess_correct': False,
            'is_user_input_valid': False}


assert game.play({'user_guessed_number': 100, 'attempts': 1})\
        == {'user_guessed_number': 0, 'cows': 0, 'bulls': 0, 'attempts': 0,
            'message': 'invalid user input', 'is_user_guess_correct': False,
            'is_user_input_valid': False}

assert game.play({'user_guessed_number': 999999, 'attempts': 1})\
        == {'user_guessed_number': 0, 'cows': 0, 'bulls': 0, 'attempts': 0,
            'message': 'invalid user input', 'is_user_guess_correct': False,
            'is_user_input_valid': False}

# TEST CASE FOR TOO HIGH GUESS

assert game.play({'user_guessed_number': 99999, 'attempts': 1})\
        == {'user_guessed_number': '99999', 'cows': 0, 'bulls': 0,
            'attempts': 2, 'message': 'too High',
            'is_user_guess_correct': False, 'is_user_input_valid': True}

# TEST CASE FOR TOO HIGH GUESS, but close
assert game.play({'user_guessed_number': 12355, 'attempts': 1})\
         == {'user_guessed_number': '12355', 'cows': 1, 'bulls': 4,
             'attempts': 2, 'message': 'too High, but close!!',
             'is_user_guess_correct': False, 'is_user_input_valid': True}

# TEST CASE FOR TOO LOW GUESS

assert game.play({'user_guessed_number': '01234', 'attempts': 1})\
        == {'user_guessed_number': '01234', 'cows': 4, 'bulls': 0,
            'attempts': 2, 'message': 'too low',
            'is_user_guess_correct': False,
            'is_user_input_valid': True}

assert game.play({'user_guessed_number': 10000, 'attempts': 1})\
        == {'user_guessed_number': '10000', 'cows': 0, 'bulls': 1,
            'attempts': 2, 'message': 'too low',
            'is_user_guess_correct': False, 'is_user_input_valid': True}

# TEST CASES FOR TOO LOW, BUT CLOSE
assert game.play({'user_guessed_number': 12330, 'attempts': 1})\
        == {'user_guessed_number': '12330', 'cows': 1, 'bulls': 3,
            'attempts': 2, 'message': 'too low, but close!!',
            'is_user_guess_correct': False,
            'is_user_input_valid': True}


# TEST CASE FOR CORRECT GUESS
assert game.play({'user_guessed_number': 12345, 'attempts': 1})\
    == {'user_guessed_number': '12345', 'cows': 0, 'bulls': 5,
        'attempts': 2, 'message': 'Great you Guessed it Correctly!!!',
        'is_user_guess_correct': True, 'is_user_input_valid': True}

# TEST CASE OF RESET

assert game.reset({'reset': True, 'length': 6})\
    == {'is_set': True, 'attempts': 0}

# TEST CASE OF exit

assert game.reset({'reset': False}) == {'message': 'Thankyou For Playing'}

print("All test cases passed!!!!")
