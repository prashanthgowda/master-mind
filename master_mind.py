import random


class MasterMind:

    def __init__(self):
        self._answer = 0
        self.max_attempts = 10
        self.result = {'user_guessed_number': 0,
                       'cows': 0,
                       'bulls': 0,
                       'attempts': 0,
                       'message': 'Error',
                       'is_user_guess_correct': False,
                       'is_user_input_valid': False}

    def initilize_answer(self, digit_length):
        if type(digit_length) == str:
            if not digit_length.isdigit():
                return {'is_set': False}

        if int(digit_length) < 2 or int(digit_length) > 9:
            return {'is_set': False}
        digit_length = int(digit_length)
        self._answer = random.randint(10**(digit_length-1), 10**digit_length)
        return {'is_set': True}

    def get_cows(self):
        cows = 0
        guessed_numbers = str(self.result['user_guessed_number'])
        answer_string = str(self._answer)
        for i in guessed_numbers:
            if i in answer_string:
                cows += 1
        cows = abs(cows - self.result['bulls'])
        self.result['cows'] = cows
        return {'cows': cows}

    def get_bulls(self):
        bulls = 0
        guessed_numbers = str(self.result['user_guessed_number'])
        answer_string = str(self._answer)
        for i in range(0, len(guessed_numbers)):
            if answer_string[i] == guessed_numbers[i]:
                bulls += 1
        self.result['bulls'] = bulls
        return {'bulls': bulls}

    def get_message(self):
        if(self.result['is_user_input_valid']):
            user_guessed_number = self.result['user_guessed_number']
            difference_in_answer = int(self._answer) - int(user_guessed_number)
            if difference_in_answer == 0:
                message = "Great you Guessed it Correctly!!!"
            elif difference_in_answer > 0:
                message = 'too low'
                if difference_in_answer < 100:
                    message += ', but close!!'
            elif difference_in_answer < 0:
                message = 'too High'
                if difference_in_answer > -100:
                    message += ', but close!!'
            if self.result['attempts'] >= self.max_attempts:
                message = 'your attempts are done sorry'
        else:
            message = "invalid user input"
        self.result['message'] = message
        return {'message': message}

    def is_correct(self):
        return self.result['user_guessed_number'] == str(self._answer)

    def is_valid_input(self, user_input):
        return (len(str(user_input)) == len(str(self._answer)) and
                str(user_input).isdigit())

    def reset(self, user_inputs):
        if 'reset' in user_inputs:
            if not user_inputs.get('reset'):
                return {'message': 'Thankyou For Playing'}
        if 'reset' in user_inputs:
            if user_inputs.get('reset'):
                if type(int(user_inputs['length'])) == int:
                    self.initilize_answer(int(user_inputs['length']))
                    self.result['attempts'] = 0
                    return {
                        'is_set': True,
                        'attempts': 0
                    }
        return {
                'is_set': False
                }

    def play(self, user_inputs):
        if len(user_inputs) != 2:
            return {
                'message': 'Error in parameters format',
                'is_parameters_valid': False
            }
        if 'user_guessed_number' in user_inputs and \
                'attempts' in user_inputs:
            user_input_num = user_inputs['user_guessed_number']
            self.result['is_user_input_valid'] = \
                self.is_valid_input(user_input_num)

            if self.result['is_user_input_valid']:
                self.result['user_guessed_number'] = str(user_input_num)
                self.result['attempts'] = user_inputs['attempts'] + 1
                self.result['is_user_guess_correct'] = self.is_correct()
                self.get_bulls()
                self.get_cows()
                self.get_message()
                return self.result
            else:
                self.get_message()
                return self.result
        return {
                'message': 'Error in input',
                'is_parameters_valid': False
                }
