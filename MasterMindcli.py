from master_mind import MasterMind
import pyfiglet

banner = pyfiglet.figlet_format("Master Mind")
print(banner)

num = str(raw_input('Enter the length of the number you want to guess\n'))

game = MasterMind()
game_init = game.initilize_answer(num)
while not game_init['is_set']:
    num = raw_input('Enter a valid input: (A digit Between 2-9)\n')
    game_init = game.initilize_answer(str(num))

play_game = True
user_inputs = {
        'user_guessed_number': 0,
        'attempts': 0
}
while play_game:
    print('_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-')
    user_inputs['user_guessed_number'] =\
        str(raw_input('Attempt {}:'.format(user_inputs['attempts']+1)))
    while user_inputs['attempts'] <= game.max_attempts:
        result = game.play(user_inputs)
        user_inputs['attempts'] = result['attempts']
        if not result['is_user_input_valid']:
            print(result['message'])
            user_inputs['user_guessed_number'] =\
                str(raw_input("Enter your guess: "))
        elif result['is_user_guess_correct']:
            print(result['message'])
            break
        elif user_inputs['attempts'] == game.max_attempts:
            print(result['message'])
            break
        else:
            print('_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-')
            print(result['message'])
            print('Bulls:{}  Cows:{}'.format(result['bulls'], result['cows']))
            print('_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-')
            user_inputs['user_guessed_number'] =\
                str(raw_input('Attempt {}:'.format(user_inputs['attempts']+1)))

    reset_input = {
        'reset': False,
        'length': 0
    }

    print("Do you want to play again?")
    play_again = str(raw_input("enter yes or y: "))
    if play_again == 'yes' or play_again == 'y':
        reset_input['reset'] = True
        num = str(raw_input('Enter the length of the number you want to guess\n'))
        game_init = game.initilize_answer(str(num))
        while not game_init['is_set']:
            num = raw_input('Enter a valid input: (A digit Between 2-9)\n')
            game_init = game.initilize_answer(str(num))
        reset_input['length'] = num
        game.reset(reset_input)
        user_inputs['attempts'] = 0
    else:
        result = game.reset(reset_input)
        banner = pyfiglet.figlet_format(result['message'])
        print(banner)
        play_game = False
